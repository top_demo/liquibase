--liquibase formatted sql

--changeset your.name:1 labels:example-label context:example-context
--comment: example comment
create table person (
    id int primary key auto_increment not null,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback DROP TABLE person;

--changeset your.name:2 labels:example-label context:example-context
--comment: example comment
create table company (
    id int primary key auto_increment not null,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback DROP TABLE company;

--changeset other.dev:3 labels:example-label context:example-context
--comment: example comment
alter table person add column country varchar(2)
--rollback ALTER TABLE person DROP COLUMN country;


--changeset other.dev:4 labels:exaple-label context:example-context
--comment: example comment
insert into person values(1,"DHARMIK","Surat","AHMEDABAD","CITY","IN")
--rollback delete from person where id = 1

--changeset other.dev:5 labels:exaple-label context:example-context
--comment: example comment
insert into company values(1,"VIRAJ","Surat","AHMEDABAD","CITY")
--rollback delete from company where id = 1

--changeset other.dev:6 labels:exaple-label context:example-context
--comment: example comment
insert into company values(2,"ABC","Surat","AHMEDABAD","CITY")
--rollback delete from company where id = 1