# liquibase

> Download From This Link
> - https://github.com/liquibase/liquibase/releases

- mkdir liquibase
- tar -zxf liquibase-4.16.1.tar.gz  --directory liquibase

- cd liquibase/

> If You use EXPORT COMMAND DONT USE ./ in command
> - export PATH=$PATH:/home/ubuntu/liquibase
- ./liquibase --version
- ./liquibase init project

> Install Drivers of JDBC
- wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.30.tar.gz
- tar -xvf mysql-connector-java-8.0.30.tar.gz 
- cp mysql-connector-java-8.0.30.jar /home/ubuntu/liquibase/lib/


- nano liquibase.properties
```
changeLogFile=example-changelog.sql
liquibase.command.url=jdbc:mysql://localhost:3306/test
liquibase.command.username: test
liquibase.command.password: 123
driver: com.mysql.cj.jdbc.Driver

url: jdbc:mysql://localhost:3306/test
username: test
password: 123
#liquibaseProLicenseKey: aeioufakekey32aeioufakekey785463214
classpath: /home/ubuntu/liquibase/lib/mysql-connector-java-8.0.30.jar

liquibase.hub.mode=off
```

## Getting started

> check Status
- ./liquibase --username=test --password=123 --changelog-file=example-changelog.sql status

> check Query
- ./liquibase  --changelog-file=example-changelog.sql update-sql

> Apply Query
- ./liquibase  --changelog-file=example-changelog.sql update

> Rollback
#./liquibase rollback-count 1
